# Программирование на C++ для начинающих

Ссылка на этот документ: https://goo-gl.su/MGXol

## Содержание

### C++

* [Введение](01-introduction.md)
* [Логический тип](02-bool.md)
* [Условный оператор](03-if.md)
* Операторы цикла
    * [for](04-01-for-02.md)
    * [while](04-02-while.md)
    * [do while](04-03-do-while.md)
* [Символьный тип](05-01-char.md)
* [Строковый тип](05-02-string.md)
* [Массивы](06-arrays.md)
* [Фукнции](07-functions.md)
* [Тип double](09-double.md)

### Raylib

* [Основы](08-raylib-basics.md)
 
## Полезные ссылки
* [ACMP - Язык программирования С++](https://acmp.ru/asp/do/index.asp?main=course&id_course=1)
* [Stepik - Введение в программирование (C++)](https://stepik.org/course/363)
* [CodeCombat](https://codecombat.com)
* [CodinGame](https://www.codingame.com)
